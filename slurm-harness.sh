#!/bin/sh
#SBATCH --job-name=harness_example       # Job name
#SBATCH --mail-type=ALL                  # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=tisaac@cc.gatech.edu # Where to send mail  
#SBATCH --time=00:05:00                  # Time limit hrs:min:sec
#SBATCH --nodes=1                        # Just one node, but
#SBATCH --exclusive                      # My node alone
#SBATCH --output=harness_example_%j.out  # Standard output and error log

pwd; hostname; date

make INFILE=lac1_novl2.xyz OUTFILE=out.xyz NINTERVALS=101

date
