#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> // access
#include <math.h>
#include <assert.h>
#include "bd.h"

int bd(int npos, double *pos, double L, const int *types, int *maxnumpairs_p, double **dist2_p, int **pairs_p)
{
  double f = sqrt(2.*DELTAT);
  /* 2 is twice the radius, $2 r_c$ in Prof. Chow's Lecture 5 */
  int     boxdim = L / 2;
  /* Must be at least the square of twice the radius */
  double  cutoff2 = 4.;
  int     maxnumpairs = *maxnumpairs_p;
  double *dist2 = *dist2_p;
  int    *pairs = *pairs_p;

  for (int step=0; step<INTERVAL_LEN; step++)
  {
    int retval;
    int numpairs = 0;

    while (1) {
      retval = interactions(npos, pos, L, boxdim, cutoff2, dist2, pairs, maxnumpairs, &numpairs);
      if (!retval) break;
      if (retval == -1) {
        free(pairs);
        free(dist2);
        maxnumpairs *= 2;
        dist2 = (double *) malloc(maxnumpairs*sizeof(double));
        pairs = (int *) malloc(2*maxnumpairs*sizeof(int));
        assert(dist2);
      } else {
        return retval;
      }
    }

    double *oldpos;
    oldpos = (double *) malloc(3 * npos * sizeof(double));

    for (int i = 0; i < 3 * npos; i++)
    {
	    oldpos[i] = pos[i];
    }

    for (int p = 0; p < numpairs; p++) {
      const double krepul = 100.;
      int p1 = pairs[2 * p];
      int p2 = pairs[2 * p + 1];
      
      double dx, dy, dz;
      if (dist2[p] < 4)
      {

        /* You are changing the pos array while you update: you could avoid
         * this by making a copy of the old positions and use that to compute
         * dx, dy, and dz */
	      dx = remainder(oldpos[3*p1+0] - oldpos[3*p2+0],L);
	      dy = remainder(oldpos[3*p1+1] - oldpos[3*p2+1],L);
	      dz = remainder(oldpos[3*p1+2] - oldpos[3*p2+2],L);
	      double s = sqrt(dist2[p]);
	      double f2 = krepul*(2.-s)*DELTAT;

#pragma omp atomic
	      pos[3*p1+0]  += f2*(dx/s);
#pragma omp atomic
	      pos[3*p1+1]  += f2*(dy/s);
#pragma omp atomic
	      pos[3*p1+2]  += f2*(dz/s);
#pragma omp atomic
	      pos[3*p2+0]  += f2*(dx/s);
#pragma omp atomic
	      pos[3*p2+1]  += f2*(dy/s);
#pragma omp atomic
	      pos[3*p2+2]  += f2*(dz/s);
      }


      /* TODO: Put the force calculation here using the same formula as exercise 04*/
      /* TODO: Take out this print statement */
#if 0
      if (!step) {
        printf("Particle pair %d: (%d, %d) are %g apart\n",p,pairs[2*p],pairs[2*p+1],sqrt(dist2[p]));
      }
#endif
    }
    // update positions with Brownian displacements
    for (int i=0; i<3*npos; i++)
    {
      double noise = cse6230nrand(nrand);

      pos[i] += f*noise;
    }
  }
  *maxnumpairs_p = maxnumpairs;
  *dist2_p = dist2;
  *pairs_p = pairs;

  return 0;
}
